
Create new rclone config with 

    docker exec -it rclone-mount rclone_setup

You can copy and move from drive to drive. Also you can create a cron job on your local machine.

Don't forget to set *server_side_across_configs = true* for a fast server to server transfer.

    docker exec -it rclone-mount rclone_copy "gdrive1:Test1/" "gdrive2:Test1/" -P
    docker exec -it rclone-mount rclone_move "gdrive1:Test1/" "gdrive2:Test1/" -P

    docker exec -it rclone-mount rclone_cmd lsd "gdrive1:" 

 rclone_cmd will passthrough all commands, execut **docker exec -it rclone-mount rclone_cmd** to see all parameters.

With **rclone_update** or **rclone_update beta** you can switch between the latest rclone version or Beta version .
   
Edit/rename **/drive/gdrive-crypt.rclone** and change *rclone_name="gdrive-crypt"* with you rclone config name, ***filename, rclone config name and rclone_name*** must be equal. 

When both are available you will get under **/rclone** a new folder with your mounted drive.

You can also have more config files under **/drive**, you must only configure rclone_setup for your drives too.

**Usage:**

    docker run -d --name rclone-mount \
        --restart=unless-stopped \
        --cap-add SYS_ADMIN \
        --device /dev/fuse \
        --security-opt apparmor:unconfined \
        -e PUID=${PUID} \
        -e PGID=${PGID} \
        -v ${USERDIR}/media:/rclone:shared \
        -v ${USERDIR}/.config/rclone/drives:/drives \
        -v ${USERDIR}/.config/rclone/config:/config \
        -v ${USERDIR}/.config/rclone/logs:/logs \
        -v ${USERDIR}/local_files:/store \
        blacknet76/rclone-mount:latest

**docker-compose.yml**


    version: "3.6"
    
    services:
      rclone-mount:
        container_name: rclone-mount
        hostname: rclone
        image: blacknet76/rclone-mount:latest
        restart: unless-stopped
        environment:
          - PUID=${PUID}
          - PGID=${PGID}
        volumes:
          - ${USERDIR}/media:/rclone:shared
          - ${USERDIR}/.config/rclone/drives:/drives
          - ${USERDIR}/.config/rclone/config:/config
          - ${USERDIR}/.config/rclone/logs:/logs
          - ${USERDIR}/local_files:/store # access local files in the container
        privileged: true
        devices:
          - /dev/fuse
        cap_add:
          - MKNOD
          - SYS_ADMIN


**Example xxx.rclone file**

    #!/usr/bin/with-contenv sh
    
    set -e
    
    . "/usr/bin/config"
    
    rclone_name="xxx-crypt" 
    
    mount_gdrive () {
            buffer_size="1G"
            dir_cache_time="72h"
            drive_chunk_size="32M"
            vfs_read_chunk_size="128M"
            vfs_read_chunk_size_limit="off"
    
            cloud_drive="${cloud_dir}/${rclone_name}"
    
            # Make sure GD mountpoint exists.
            if [ ! -d "${cloud_drive}" ]; then
                    mkdir -p "${cloud_drive}"
                    chown -R $PUID:$GUID "${cloud_drive}"
            fi
    
            # Mount GD if not already mounted.
            if [ $(ps -ef | grep "rclone.*$rclone_name" | grep -v grep | wc -l) == "0" ]
               then
                    echo "[ $(date $(printenv DATE_FORMAT)) ] Mounting Google Drive mountpoint: ${cloud_drive}" >> "${log_dir}/rmount.log";
                    rclone mount ${rclone_name}: "${cloud_drive}" \
                      ${rclone_config} \
                            --allow-other \
                            --buffer-size $buffer_size \
                            --dir-cache-time $dir_cache_time \
                            --drive-chunk-size $drive_chunk_size \
                            --fast-list \
                            --umask 002 \
                            --uid ${_puid:-911} \
                            --gid ${_pgid:-911} \
                            --vfs-read-chunk-size $vfs_read_chunk_size \
                            --vfs-read-chunk-size-limit $vfs_read_chunk_size_limit &
               #else
                    #echo "[ $(date $(printenv DATE_FORMAT)) ] Google Drive mountpoint: ${cloud_drive} already mounted."
            fi
    }
    
    ###############################################################################
    
    if [ $(ps -ef | grep $rclone_name | grep -v grep | wc -l) == "1" ]
       then
            echo "[ $(date $(printenv DATE_FORMAT)) ] Mount already in progress. Aborting."
       else
            mount_gdrive
    fi
    
    exit 0

**Example: rclone.conf**

    [xxx]
    type = drive
    client_id = <CLIENT_ID>
    client_secret = <CLIENT_SECRET>
    scope = drive
    token = {"access_token":"XXXXXXXXX","token_ty
    pe":"Bearer","refresh_token":"XXXXXX","expiry":"2019-05-14T21:19:59.961494956Z"}
    
    [xxx-crypt]
    type = crypt
    remote = gdrive:rclone
    filename_encryption = standard
    directory_name_encryption = true
    password = ************************
    password2 = ************************


