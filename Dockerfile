FROM alpine:latest

ENV DATE_FORMAT "+%F@%T"
ENV PLATFORM_ARCH="amd64"
ARG RCLONE_VERSION="v1.49.2"

# s6 environment settings
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2
ENV S6_KEEP_ENV=1

# install packages
RUN \
 apk update && \
 apk add --no-cache \
 ca-certificates \
 fuse && \
 sed -i 's/#user_allow_other/user_allow_other/' /etc/fuse.conf

# install build packages
RUN \
 apk add --no-cache --virtual=build-dependencies \
	vim \
	wget \
	curl \
	unzip && \
# add s6 overlay
 OVERLAY_VERSION=$(curl -sX GET "https://api.github.com/repos/just-containers/s6-overlay/releases/latest" \
	| awk '/tag_name/{print $4;exit}' FS='[""]') && \
 curl -o \
	/tmp/s6-overlay.tar.gz -L \
	"https://github.com/just-containers/s6-overlay/releases/download/${OVERLAY_VERSION}/s6-overlay-${PLATFORM_ARCH}.tar.gz" && \
 tar xfz \
	/tmp/s6-overlay.tar.gz -C / 
# install rclone   
RUN cd tmp && \
 wget -q https://downloads.rclone.org/${RCLONE_VERSION}/rclone-${RCLONE_VERSION}-linux-${PLATFORM_ARCH}.zip && \
 unzip /tmp/rclone-${RCLONE_VERSION}-linux-${PLATFORM_ARCH}.zip && \
 mv /tmp/rclone-${RCLONE_VERSION}-linux-${PLATFORM_ARCH}/rclone /usr/bin && \
 sed -i 's/#user_allow_other/user_allow_other/' /etc/fuse.conf && \
 echo -e 'alias rclone="rclone --config=/config/rclone.conf"' | tee -a ~/.bashrc > /dev/null 2>&1 

RUN echo '. /etc/profile;' >> /root/.profile \
    && echo -e 'alias rclone="rclone --config=/config/rclone.conf"' | tee -a /etc/profile > /dev/null 2>&1 \
    && echo -e 'alias ll="ls -la --color"' | tee -a /etc/profile > /dev/null 2>&1

RUN apk add --no-cache --repository http://nl.alpinelinux.org/alpine/edge/community \
	shadow && \
# cleanup
 apk del --purge \
	build-dependencies && \
 rm -rf \
	/tmp/* \
	/var/tmp/* \
	/var/cache/apk/*

# create abc user
RUN \
	groupmod -g 1000 users && \
	useradd -u 911 -U -d /config -s /bin/false abc && \
	usermod -G users abc && \
# create some files / folders
	mkdir -p /config /logs /rclone /drives && \
	touch /var/lock/rclone.lock

# add local files
COPY root /
COPY drives/ /drives
COPY scripts/* /usr/bin/
RUN chmod +x /usr/bin/check \
             /usr/bin/config \
             /usr/bin/rclone_* \
             /usr/bin/remount \
             /usr/bin/rmount \
	     /usr/bin/unmount

VOLUME /rclone /drives /config /logs /store

RUN chmod -R 777 /drives /rclone /logs /store

WORKDIR /rclone

ENTRYPOINT ["/init"]
